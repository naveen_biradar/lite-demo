class CreateAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :accounts do |t|
      t.string :account_type
      t.date :opening
      t.references :user, foreign_key: true
      t.string :branch
      t.string :minorIndicator

      t.timestamps
    end
  end
end
