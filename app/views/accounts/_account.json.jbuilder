json.extract! account, :id, :account_type, :opening, :user_id, :branch, :minorIndicator, :created_at, :updated_at
json.url account_url(account, format: :json)
